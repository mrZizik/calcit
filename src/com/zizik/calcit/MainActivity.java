package com.zizik.calcit;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
	
	int a=0,b=0;
	char c=' ';
	float r=0;
	boolean aInit=false,bInit=false;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	public void but1(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+1;
			editText.setText(""+a);
		} else 
		{
			b=b*10+1;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but2(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+2;
			editText.setText(""+a);
		} else 
		{
			b=b*10+2;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	
	public void but3(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+3;
			editText.setText(""+a);
		} else 
		{
			b=b*10+3;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but4(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+4;
			editText.setText(""+a);
		} else 
		{
			b=b*10+4;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but5(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+5;
			editText.setText(""+a);
		} else 
		{
			b=b*10+5;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but6(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+6;
			editText.setText(""+a);
		} else 
		{
			b=b*10+6;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but7(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+7;
			editText.setText(""+a);
		} else 
		{
			b=b*10+7;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but8(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+8;
			editText.setText(""+a);
		} else 
		{
			b=b*10+8;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but9(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10+9;
			editText.setText(""+a);
		} else 
		{
			b=b*10+9;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	public void but0(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (!aInit) {
			a=a*10;
			editText.setText(""+a);
		} else 
		{
			b=b*10;
			editText.setText(""+b);
			bInit=true;
		}
		
	}
	
	
	public void plus(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		c='+';
		editText.setText(""+c);
		aInit=true;
	}
	
	public void minus(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		c='-';
		editText.setText(""+c);
		aInit=true;
	}
	
	
	public void multiply(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		c='*';
		editText.setText(""+c);
		aInit=true;
	}
	
	public void devide(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		c='/';
		editText.setText(""+c);
		
		aInit=true;
	}
	
	public void equal(View view) {
		EditText editText = (EditText) findViewById(R.id.display);
		if (aInit && bInit) {
			switch (c) {
			case '+': r=a+b; editText.setText(""+r); break;
			case '-': r=a-b; editText.setText(""+r); break;
			case '*': r=a*b; editText.setText(""+r); break;
			case '/': r= (float)a/b; editText.setText(""+r); break;
			}
			a=b=0;
			aInit=bInit=false;
		}
		else {
			editText.setText("������� �����");
			a=b=0;
			aInit=bInit=false;
		}
				
	}
	
	public void CE (View view) {
		a=b=0;
		aInit=bInit=false;
		EditText editText = (EditText) findViewById(R.id.display);
		editText.setText(null);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
